[![Build Status](https://drone.io/bitbucket.org/BanksySan/cmd-line-args/status.png)](https://drone.io/bitbucket.org/BanksySan/cmd-line-args/latest)

> Clean parsing of command line arguments

**_Not production ready_**

## Install

```sh
$ npm install --save cmd-line-args
```


## Usage

```js
var args = require('cmd-line-args');

var appPath = args.applicationPath;
```


## License

MIT © [David Banks]()
