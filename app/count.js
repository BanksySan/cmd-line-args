var config = require('./config'),
    _ = config.underscore;

module.exports = function (argsv, s, callback) {
    var i, count = 0;
    if (!_.isString(s)) {
        callback(new Error('Expected a string for count but got ' + s), null);
    } else {
        for (i = 0; i < argsv.length; i++) {
            if (argsv[i] === s) {
                count++;
            }
        }

        callback(null, count);
    }
};
