var config = require('./config'),
    _ = config.underscore;

module.exports = function (argsv, s, callback) {
    if (!_.isString(s)) {
        callback(new Error('Expected a string to search for but got ' + s), null);
    } else {
        var isContaining = _.contains(argsv, s);
        callback(null, isContaining);
    }
};
