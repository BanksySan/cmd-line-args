'use strict';
var verify = require('./verify'),
    count = require('./count'),
    exists = require('./exists'),
    named = require('./named-arguments');

module.exports = {
    parse: function (args, callback) {
        verify(args, function (error) {
            if (error) {
                callback(error);
            }
            else {
                named(args, function (namedArgs) {


                    callback(null, {
                        applicationPath: args[1],
                        exists: exists,
                        count: count,
                        namedArguments: namedArgs
                    });
                });
            }
        });
    }
};
