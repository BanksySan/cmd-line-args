'use strict';

module.exports = function (args, callback) {
    var _ = require('underscore'),
        namedArgs = [];

    _.forEach(args, function (item) {
        var splitChar = '=',
            index = item.indexOf(splitChar),
            key, value;

        if (index === -1) {
            key = item;
            value = null;
        } else {
            key = item.substring(0, index);
            value = item.substr(index + 1);
        }

        var namedArg = _.find(namedArgs, function (item) {
            return item.key === key;
        });

        if (!namedArg) {
            namedArg = {key: key, value: []};
            namedArgs.push(namedArg);
        }

        namedArg.value.push(value);
    });

    callback(null, {
        item: function (name) {
            return _.find(namedArgs, function (item) {
                return item.key === name;
            });
        }
    });
};


