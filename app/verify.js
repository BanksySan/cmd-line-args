var _ = require('underscore');

module.exports = function (processArguments, callback) {
    if (processArguments === null || processArguments === undefined) {
        callback(new Error('argv is null or undefined'));
    } else if (!_.isArray(processArguments)) {
        callback(new Error('Expected argv array to be passed'));
    } else if (processArguments.length < 2) {
        callback(new Error('argv must has at least two elements.'));
    } else if (processArguments[0] !== 'node') {
        callback(new Error('argv has the first element equal to "node"'));
    } else {
        callback();
    }
};
