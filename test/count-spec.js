'use strict';

describe('the count function', function () {
    var helper = require('./test-helper'),
        count = helper.modules.count;

    require('should');

    describe('return an error', function () {
        var invokeTest = function (done, arg) {
            var args = helper.makeArgs([arg]),
                expectedErrorMessage = 'Expected a string for count but got ' + arg;
            count(args, arg, function (e, count) {
                (count === null).should.equal(true);
                e.message.should.equal(expectedErrorMessage);
                done();
            });
        };

        it('if it is a number', function (done) {
            var arg = 1;
            invokeTest(done, arg);
        });

        it('if it is a regex', function (done) {
            var arg = /regex/;
            invokeTest(done, arg);
        });

        it('if it is a boolean', function (done) {
            var arg = true,
                expectedErrorMessage = 'Expected a string to search for but got ' + arg;
            invokeTest(done, arg, expectedErrorMessage);
        });

        it('if it is a null', function (done) {
            var arg = null,
                expectedErrorMessage = 'Expected a string to search for but got ' + arg;
            invokeTest(done, arg, expectedErrorMessage);
        });

        it('if it is a undefined', function (done) {

            var expectedErrorMessage = 'Expected a string to search for but got ' + undefined;
            invokeTest(done, undefined, expectedErrorMessage);
        });
    });

    describe('will correctly count the number of instances', function () {
        describe('returns the correct count', function () {
            var dummyItem1, dummyItem2, otherItem, args;

            beforeEach(function () {
                dummyItem1 = 'dummy-item-1';
                dummyItem2 = 'dummy-item-2';
                otherItem = 'other-item';
                args = helper.makeArgs([dummyItem1, dummyItem2, dummyItem1]);
            });

            it('gives two when there are two items', function (done) {
                count(args, dummyItem1, function (e, count) {
                    (e === null).should.equal(true);
                    count.should.equal(2);
                    done();
                });
            });

            it('gives one when there is one item', function (done) {
                count(args, dummyItem2, function (e, count) {
                    (e === null).should.equal(true);
                    count.should.equal(1);
                    done();
                });
            });

            it('gives zero when there are no items', function (done) {
                count(args, otherItem, function (e, count) {
                    (e === null).should.equal(true);
                    count.should.equal(0);
                    done();
                });
            });
        });
    });
});
