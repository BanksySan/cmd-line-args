'use strict';

describe('The exists module', function () {
    var helper = require('./test-helper'),
        exists = helper.modules.exists;

    describe('return an error', function () {

        var invokeTest = function (done, arg, expectedErrorMessage) {
            var args = helper.makeArgs(arg);

            exists(args, arg, function (e, o) {
                (o === null).should.equal(true);
                e.message.should.equal(expectedErrorMessage);
                done();
            });
        };

        it('if it is a number', function (done) {
            var arg = 1, expectedErrorMessage = 'Expected a string to search for but got ' + arg;
            invokeTest(done, arg, expectedErrorMessage);
        });

        it('if it is a regex', function (done) {
            var arg = /regex/,
                expectedErrorMessage = 'Expected a string to search for but got ' + arg;
            invokeTest(done, arg, expectedErrorMessage);
        });

        it('if it is a boolean', function (done) {
            var arg = true,
                expectedErrorMessage = 'Expected a string to search for but got ' + arg;
            invokeTest(done, arg, expectedErrorMessage);
        });

        it('if it is a null', function (done) {
            var arg = null,
                expectedErrorMessage = 'Expected a string to search for but got ' + arg;
            invokeTest(done, arg, expectedErrorMessage);
        });

        it('if it is a undefined', function (done) {

            var expectedErrorMessage = 'Expected a string to search for but got ' + undefined;
            invokeTest(done, undefined, expectedErrorMessage);
        });
    });
    describe('correctly return if an item exists', function () {
        describe('if all items are unique', function () {

            var dummyString1 = '1',
                dummyString2 = 'dummy-string',
                args = helper.makeArgs([dummyString1, dummyString2]);

            it('returns one for the first argument', function (done) {
                exists(args, dummyString1, function (e, exists) {
                    exists.should.equal(true);
                    done();
                });
            });

            it('returns one for the second argument', function (done) {
                exists(args, dummyString1, function (e, exists) {
                    exists.should.equal(true);
                    done();
                });
            });

            it('returns zero for something else', function (done) {
                exists(args, 'something else', function (e, exists) {
                    exists.should.equal(false);
                    done();
                });
            });
        });

        describe('if items contain duplicates', function () {
            var duplicatedArgument = 'dummy-string';
            var cmdArgs = [duplicatedArgument, duplicatedArgument];
            var args = helper.makeArgs(cmdArgs);

            it('returns true for single item', function (done) {
                exists(args, duplicatedArgument, function (e, exists) {
                    exists.should.equal(true);
                    done();
                });
            });

            it('returns true for duplicated item item', function (done) {
                exists(args, 'something else', function (e, exists) {
                    exists.should.equal(false);
                    done();
                });
            });
        });
    });
});
