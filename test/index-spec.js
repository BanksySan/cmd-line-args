'use strict';

describe('The parser module', function () {
    var helper = require('./test-helper'),
        proxyquire = helper.proxyQuire;

    require('should');

    describe('If verifications fails', function () {

        it('the exception is returned in the callback', function (done) {
            var dummyError = new Error('dummy-error'),
                stubs = {
                    './verify': function (args, callback) {
                        callback(dummyError);
                    }
                };
            var parser = proxyquire('../app/index', stubs);

            parser.parse([1, 2, 3], function (error) {
                error.message.should.equal(dummyError.message);
                done();
            });
        });
    });

    describe('If verification succeeds', function () {
        var parser, baseArgs = helper.baseArgs;

        beforeEach(function () {
            parser = proxyquire('../app/index', {
                './verify': function (args, callback) {
                    callback();
                }
            });
        });

        it('returns the correct application path', function (done) {
            parser.parse(baseArgs, function (e, parsedArgs) {
                parsedArgs.applicationPath.should.be.equal(helper.applicationPath);
                done();
            });
        });
    });
});
