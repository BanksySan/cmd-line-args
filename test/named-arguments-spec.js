'use strict';
describe('The named arguments module', function () {
    var helper = require('./test-helper'),
        namedArguments = helper.modules.namedArguments;
    describe('when passed an argument split by an equals sign', function () {

        var key1 = 'key1',
            key2 = 'key2',
            value1 = 'value1',
            value2 = 'value2',
            dummyArgs = [key1 + '=' + value1, key2 + '=' + value2],
            parsedArgs;

        before(function (done) {
            namedArguments(dummyArgs, function (e, i) {
                parsedArgs = i;
                done();
            });
        });

        it('initialises correctly', function () {
            parsedArgs.should.not.equal(null);
            parsedArgs.should.not.equal(undefined);
        });

        describe('the returned named argument parser', function () {
            it('is not implemented', function (done) {
                var arg1 = key1 + '=' + value1,
                    arg2 = key1 + '=' + value2,
                    args = [arg1, arg2];

                namedArguments(args, function (e, namedArgs) {
                    var namedArg = namedArgs.item(key1),
                        values = namedArg.value;
                    values.length.should.equal(2);

                    done();
                });
            });
        });
    });
});

