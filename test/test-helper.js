'use strict';

module.exports = (function () {
    var applicationPath = 'dummy-application-path',
        appDirectory = '../app/',
        baseArgs = ['node', applicationPath],
        modules = {
            index: require(appDirectory + 'index'),
            verify: require(appDirectory + 'verify'),
            count: require(appDirectory + 'count'),
            exists: require(appDirectory + 'exists'),
            namedArguments: require(appDirectory + 'named-arguments'),
            sinon: require('sinon'),
            underscore: require('underscore')
        };

    return {
        modules: modules,
        baseArgs: baseArgs,
        makeArgs: function (args) {
            return baseArgs.concat(args);
        },
        proxyQuire: require('proxyquire'),
        applicationPath: applicationPath
    };
})();
