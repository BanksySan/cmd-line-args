'use strict';

describe('The verifying component', function () {
    require('should');
    var helper = require('./test-helper'),
        verify = helper.modules.verify;

    describe('must return an error', function () {
        it('if null is passed', function (done) {
            verify(null, function (error) {
                error.message.should.equal('argv is null or undefined');
                done();
            });
        });

        it('if undefined is passed', function (done) {
            verify(undefined, function (error) {
                error.message.should.equal('argv is null or undefined');
                done();
            });
        });

        it('array is too short', function (done) {
            verify([1], function (error) {
                error.message.should.equal('argv must has at least two elements.');
                done();
            });
        });

        it('first element is not "node".', function (done) {
            verify(['not-node', 1], function (error) {
                error.message.should.equal('argv has the first element equal to "node"');
                done();
            });
        });
    });

    describe('When passed a valid argv', function () {
        it('returns without error', function (done) {
            var args = helper.baseArgs;
            verify(args, done);
        });
    });
});
